import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {MySequence} from './sequence';

export {ApplicationConfig};

export class OrderTheMonthsApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);
    const months = ["May", "February", "June", "August", "September", "December", "Junuary", "March", "October", "November", "July", "April"];

    const orderMonths =  (months: string[]): any[] => {
      let resp: any[] = [];
      months.map(month => {
        let value = month.toLocaleLowerCase();
        switch (value) {
          case 'junuary':
            resp[0] = month;
            break;
          case 'february':
            resp[1] = month;
            break;
          case 'march':
            resp[2] = month;
            break;
          case 'april':
            resp[3] = month;
            break;
          case 'may':
            resp[4] = month;
            break;
          case 'june':
            resp[5] = month;
            break;
          case 'july':
            resp[6] = month;
            break;
          case 'august':
            resp[7] = month;
            break;
          case 'september':
            resp[8] = month;
            break;
          case 'october':
            resp[9] = month;
            break;
          case 'november':
            resp[10] = month;
            break;
          case 'december':
            resp[11] = month;
            break;
        }
      })
      return resp;
    }

    let order = orderMonths(months);
    console.log(order);


    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

}
